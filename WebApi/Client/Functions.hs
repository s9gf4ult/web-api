module WebApi.Client.Functions
       ( -- * Handler functions
         queryJson
       ) where

import Blaze.ByteString.Builder
import Network.HTTP.Conduit
import Network.HTTP.Types
import WebApi.Client.Types
import WebApi.Common.Types

-- | Perform query encoding, running http query and parsing response.
-- Here is usage example:
--
-- @
-- let route = Api1NoteUpdate noteId
--     rdata = NUReq
--             $ NoteUpdate Nothing (Just "text")
-- NUResp note <- queryJson manager req route rdata
--                >>= handleErrors
-- @
--
-- That's it. Note, that generating wrong request, or parsing response to
-- wrong type will never happen because of types.
queryJson :: (JsonEndpoint a)
          => Manager             -- ^ Http manager
          -> Request             -- ^ Initial request with host:port and
          -- other parameters. This method will replace just path, query,
          -- method and checkStatus fields
          -> a              -- ^ endpoint itself
          -> Req a          -- ^ request data
          -> IO (JResponse a)
queryJson man r endp rdata = do
    let req' = encodeJsonRequest rdata r
        req = req' { checkStatus = \_ _ _ -> Nothing
                   , method = endpointMethod endp
                   , path = toByteString
                            $ encodePathSegments
                            $ routePath endp
                   , requestHeaders =
                       ("accept", "application/json")
                       : requestHeaders req'
                   }
    resp <- httpLbs req man
    return $ parseJsonResponse resp
