module WebApi.Tutorial
       (
         -- * Common code
         -- $common

         -- * Client side
         -- $client

         -- * Server side
         -- $server
       ) where


{- $common

For example, you have some service for managing user notes. It can create,
read, update and delete user notes. You have to create two packages :
__notes-server__ and __notes-client__. Client will talk with server by JSON
api, so, on server side you have routes like that:

@
\/api\/v1\/notes                NotesR       GET
\/api\/v1\/notes\/public         NotesPublicR GET
\/api\/v1\/notes\/create         NotesCreateR POST
\/api\/v1\/note\/#NoteId         NoteR        GET
\/api\/v1\/note\/#NoteId\/update  NoteUpdateR  POST
\/api\/v1\/note\/#NoteId\/delete  NoteDeleteR  POST
@

This example is for Yesod, but __web-api__ does not depend on Yesod or any
other web server. But __web-api__ depends on __http-conduit__ which is the
default client implementation.

Lets consider just one endpoint, note updating for example. To update note
you need several things:

* Generate correct path on client side
* Generate request body with data to update note on client side
* Parse request body on server side to understand what client wants
* Generate response on server side
* Parse server response on client type to understand what happened on server
  and get data.
* You want to do it type safe so the type used to genrate request on client
  side must be at least isomorphic to type on server side. And vise versa.

We want to have one type for request data on client and server side, so
let's move this type to separate package and import this package on both,
server and client. Call this package __notes-api__.

Here is data which we will use for query:

@
data NoteUpdate =
    NoteUpdate
    { nuPublicity :: !(Maybe Bool)
    , nuText      :: !(Maybe Text)
    } deriving (Eq, Ord, Show, Read, Typeable)
@

What this type tells us? To update note we must give the server optional
publicity flag and optional note's text. After successfull note updating
server must give us updated note:

@
data UserNote =
    UserNote
    { noteId        :: Text
    , noteUserId    :: Text
    , notePublicity :: Bool
    , noteText      :: Text
    } deriving (Eq, Ord, Show, Read, Typeable)
@

We also want to generate the path __with note id__ in it, so we create type
like that:

@
data Api1NoteUpdate =
    Api1NoteUpdate
    { nuNoteId :: !Text
    } deriving (Ord, Eq, Read, Show, Typeable)
@

Now go next, lets define typeclass to stick request/response types to this
route:

@
class Endpoint a where
    data Req a      :: *
    data Resp a     :: *
    type ErrResp a  :: *
    endpointPath    :: a -> [Text]
    endpointMethod  :: a -> Method
@

and implement an instance for our route type:

@
data EntityResponse a =
    EntityResponse
    { erResult  :: !Bool
    , erReply   :: !a
    , erContext :: !Text
    } deriving (Ord, Eq, Show, Read, Typeable)

data ErrorResponse
    = ERUnknown
    | ERServerError { erMessage :: Text }
    | ERTeapot { erVolume :: Double }

instance Endpoint Api1NoteUpdate where
    newtype Req Api1NoteUpdate =
        NUReq NoteUpdate
        deriving (Ord, Eq, FromJSON, ToJSON)
    newtype Resp Api1NoteUpdate =
        NUResp (EntityResponse UserNote)
        deriving (Ord, Eq, FromJSON, ToJSON)
    type ErrResp Api1NoteUpdate = ErrorResponse
    endpointPath a =
        [ \"api\", \"v1\", \"note\"
        , nuNoteId a
        , \"update\" ]
    endpointMethod _ = \"POST\"
@

Now we can generate path to our endpoint, we know which method to use,
and function with type like:

@
function :: Req a -> IO (Resp a)
@

will not be ambigous because we used data families in 'Endpoint'
typeclass.

All this code must be placed in common package called __notes-api__ which is imported in both __note-server__ and __notes-client__ packages.

-}

{- $client

Now we need to encode\/decode request\/response on client side. We do it
by instantiating this typeclass:

@
class Endpoint a => JsonEndpoint a where
    type JEErr a :: *
    encodeJsonRequest :: Req a
                      -> Request
                      -> Request
    parseJsonResponse :: Response BL.ByteString -- ^ Server response
                      -> JResponse a

type EPResponse a = Either (ErrResp a) (Resp a)
type JResponse a = Either (JEErr a) (EPResponse a)
@

Here 'JEErr' is a type of request parsing error. If you are using aeson to
parse JSON response this type will be 'String', other parsers can return
more structured error description.

Lets instantiatate this typeclass for our route:

@
instance JsonEndpoint Api1NoteUpdate where
    type JEErr Api1NoteUpdate = String
    encodeJsonRequest rdata req =
        req { requestHeaders =
                      ("content-type", "application/json")
                      : requestHeaders req
            , requestBody = RequestBodyLBS
                            $ encode rdata
            }

    parseJsonResponse resp =
        case statusCode $ responseStatus resp of
            200 -> tryParse resp Right
            _   -> tryParse resp Left
      where
        tryParse r side = side
                          <$> eitherDecode (responseBody r)
@

In real world we would use default implementations of method
'encodeJsonRequest' and 'parseJsonResponse' provided in module
"WebApi.Client.Types", but it would be less visually.

Thsats it. We can do requests to the server using function

@
queryJson :: (JsonEndpoint a)
          => Manager             -- ^ Http manager
          -> Request             -- ^ Initial request with host:port and
          -- other parameters. This method will replace just path, query,
          -- method and checkStatus fields
          -> a              -- ^ endpoint itself
          -> Req a          -- ^ request data
          -> IO (JResponse a)
@

like that:

@
let route = Api1NoteUpdate noteId
    rdata = NUReq
            $ NoteUpdate Nothing (Just "text")
NUResp note <- queryJson manager req route rdata
               >>= handleErrors
@

where 'handleErrors' is just some function of such type:

@
handleErrors :: Either (JEErr a) (Either (ErrResp a) (Resp a))
             -> IO (Resp a)
@

which handles parser or server error or return successfull request.

Note, that types of __rdata__ and __note__ are sticked to __NoteUpdate__
and __(EntityResponse UserNote)__ respectively just because the type of
__route__ is __Api1NoteUpdate__. So, our client is completely safe!

-}

{- $server

To handle this request on server side we need to parse request and generate
response. Server side handlers is usually a reader monad, like in Yesod and
Scotty, which gives an access to the request.

@
class (Monad m) => JsonHandler m a where
    type JHErr a :: *
    parseRequest :: m (JHRequest a)
    encodeResponse :: JHResponse a -> m ()

type JHRequest a = Either (JHErr a) (Req a)
type JHResponse a = Either (ErrResp a) (Resp a)
@

This is typeclass of two parameters: handler monad which has methods to
access request and route type. Here is an instance:

@
instance JsonHandler Handler Api1NoteUpdate where
    type JHErr Api1NoteUpdate = String
    parseRequest = do
        res <- parseJsonBody
        return $ case res of
            (Error e) -> Left e
            (Success a) -> Right a
    encodeResponse = either
                     atsResponse
                     (sendResponse . toJSON)
      where
        atsResponse a = sendResponseStatus
                        (atsStatus a)
                        $ toJSON a
          where
            atsStatus = error "Implement: status generation function"
@

Using this simple usefull function:

@
handleJson :: (JsonHandler m a)
           => (JHRequest a -> m (JHResponse a))
           -> m ()
handleJson action = do
    req <- parseRequest
    resp <- action req
    encodeResponse resp
@

we can constraint types and implement safe server side handler:

@
postNoteUpdateR :: NoteId -> Handler ()
postNoteUpdateR nid = handleJson $ \ereq -> runExceptT $ do
    NUReq noteUpdate <- either
                        parserError
                        return
                        ereq
    .........
    -- performing note updating
    .........

    return $ NUResp
           $ EntityResponse
           { erResult = True
           , erReply  = UserNote { noteId = nid
                                 , noteUserId = uid
                                 , notePublicity = publicity
                                 , noteText = noteText }
           , erContext = "note" }
@

Note, that because of pattern matching with __NUReq__ in the begining __noteUpdate__ has a type __NoteUpdate__ and response is __(EntityResponse UserNote)__. Handler will not compiled if we would used other types.

-}
