module WebApi.Common.Types
       ( -- * typeclasses
         Route(..)
       , Endpoint(..)
       , EPResponse
       ) where

import Data.Text ( Text )
import Data.Typeable ( Typeable )
import Network.HTTP.Types

-- | Instances of this typeclass have as path on some service
class Route a where
    -- | Method to generate path to the endpoint on client side. This code
    -- is usually used for client.
    routePath :: a -> [Text]

-- | Each endpoint of your API is a couple of path and method. This
-- typeclass assigns types of request and both responses to endpoint. You
-- need to define your own data type and instantiate this
-- typeclass. Instance will be used on both server and client side.
--
-- Here is example of implementation:
--
-- @
-- data NoteUpdate =
--     NoteUpdate
--     { nuPublicity :: !(Maybe Bool)
--     , nuText      :: !(Maybe Text)
--     } deriving (Eq, Ord, Show, Read, Typeable)
--
-- data EntityResponse a =
--     EntityResponse
--     { erResult  :: !Bool         -- ^ always must be true
--     , erReply   :: !a
--     , erContext :: !Text         -- ^ name of type of object
--     } deriving (Ord, Eq, Show, Read, Typeable)
--
-- data UserNote =
--     UserNote
--     { noteId        :: Text
--     , noteUserId    :: Text
--     , notePublicity :: Bool
--     , noteText      :: Text
--     } deriving (Eq, Ord, Show, Read, Typeable)
--
-- data Api1NoteUpdate =
--     Api1NoteUpdate
--     { nuNoteId :: !Text
--     } deriving (Ord, Eq, Read, Show, Typeable)
--
-- instance Endpoint Api1NoteUpdate where
--     -- | Here we specify that request to the server must be 'NoteUpdate'
--     -- which has 'FromJSON' and 'ToJSON' instances. This condition will be
--     -- respected on server side as well as on client side.
--     newtype Req Api1NoteUpdate =
--         NUReq NoteUpdate
--         deriving (Ord, Eq, FromJSON, ToJSON)
--
--     -- | Successfull response from server must be type (EntityResponse
--     -- UserNote) as specified in this instance.
--     newtype Resp Api1NoteUpdate =
--         NUResp (EntityResponse UserNote)
--         deriving (Ord, Eq, FromJSON, ToJSON)
--
--     -- | Error response from the server must be 'ATSErrorResponse'.
--     type ErrResp Api1NoteUpdate = ATSErrorResponse
--     endpointPath a =
--         [ \"api\", \"v1\", \"note\"
--         , nuNoteId a
--         , \"update\" ]
--     endpointMethod _ = \"POST\"
-- @
--
class (Route a) => Endpoint a where
    -- | Data of this type must generated on client side and parsed on
    -- server side. Type can be encoded as JSON, list of parameters, or
    -- XML, this does not matters for this typeclass.
    data Req a      :: *

    -- | Data of this type generated on server side as successfull response
    -- and parsed on client side. Typically, response considered
    -- successfull when http status is 200.
    data Resp a     :: *

    -- | Data of this type is generated on server side as error response
    -- and parsed on client side. Typically, response considered to be
    -- error response when http status is not 200.
    type ErrResp a  :: *

    -- | Returns http method of request.
    endpointMethod  :: a -> Method

deriving instance Typeable Req
deriving instance Typeable Resp


-- | Parsed response from server. Which is either server error description
-- or successfull response
type EPResponse a = Either (ErrResp a) (Resp a)
